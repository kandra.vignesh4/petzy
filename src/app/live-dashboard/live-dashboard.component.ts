import { AUTO_STYLE } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-live-dashboard',
  templateUrl: './live-dashboard.component.html',
  styleUrls: ['./live-dashboard.component.css']
})
export class LiveDashboardComponent implements OnInit {

  constructor() { }
  title = 'Stats of November';
  type = 'LineChart';
  data = [
       ["12pm",  3],
       ["1pm",  3],
       ["2pm",  11],
       ["3pm",  21],
       ["4pm",  7],
       ["5pm",  12],
       ["6pm",  6]
  ];
  columnNames = ["Time", "Calls"];
  options = {
     hAxis: {
        title: 'Time[November 18, 2020]'
     },
     vAxis:{
        title: 'Calls'
     },
   pointSize:5
  };
  width =900;
  height = 500;



  ngOnInit(): void {



  }

}
